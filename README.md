 Ph.D Thesis
====================
 
This Repository contains the compiled Ph.D thesis. This thesis contains my own work,
therefore in case of being used, please provide the proper **citation**. 



### Citation


```
#!bibtex
@PhdThesis{Vasquez2020,
  author      = {Luis Vasquez},
  date        = {2020-04-30},
  institution = {Lodz University of Technology},
  title       = {Insight into vapour-pressure isotope effects by quantum chemistry methods},
  language    = {English},
  location    = {Lodz, Poland},
  pagetotal   = {149},
  type        = {phdthesis},
  url         = {https://bitbucket.org/apolionl/thesis},
}	
```
- - -